﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour {


    public LevelManager levelManager;
    public Animation anim;
    public float run;

	// Use this for initialization
	void Start () 
    {
        levelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //Putting the player in the active checkpoint
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Character")
        {
            levelManager.respawnPlayer();
        }
    }
}
